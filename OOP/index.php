<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");


// $sheep = new Animal("shaun");

// echo "Name : " . $sheep->name; // "shaun"
// echo "<br>";
// echo "legs : " . $sheep->legs; // 4
// echo "<br>";
// echo "cold_blooded : " . $sheep->cold_blooded; // "no"
// echo "<br>";
// echo "<br>";

// $kodok = new Frog("buduk");
// echo "Name : " . $kodok->name;
// echo "<br>";
// echo "legs : " .$kodok->legs;
// echo "<br>";
// echo "cold_blooded : " . $kodok->cold_blooded;
// echo "<br>";
// echo "Jump : " . $kodok->jump();
// echo "<br>";
// echo "<br>";

// $kera = new Ape("Kera Sakti");
// echo "Name : " . $kera->name;
// echo "<br>";
// echo "legs : " .$kera->legs;
// echo "<br>";
// echo "cold_blooded : " . $kera->cold_blooded;
// echo "<br>";
// echo "Yell : " .$kera->yell();

$sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

?>