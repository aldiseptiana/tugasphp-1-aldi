<?php

require_once ("Animal.php");

class Ape extends Animal{
    public $legs = 2;
    public $yell = "Auooo";

    public function yell(){

        echo "Yell : " . $this->yell . "<br>";
    }

}

?>