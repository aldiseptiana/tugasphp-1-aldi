1. Membuat Database

create database myshop;

2. Membuat Table di Dalam Database

CREATE TABLE users(
     id int primary key auto_increment,
     name varchar(255),
     email varchar(255),
     password varchar(255)
     );

CREATE TABLE categories(
     id int primary key auto_increment,
     name varchar(255)
     );

CREATE TABLE items(
     id int primary key auto_increment,
     name varchar(255),
     description varchar(255),
     price int not null,
     stock int not null,
     category_id int not null,
     foreign key(category_id) references categories(id)
     );


3.Memasukkan Data pada Table

insert into users(name, email, password) values
     ("John Doe", "john@doe.com", "john123"),
     ("Jane Doe", "jane@doe.com", "jenita123");

insert into categories(name) values
     ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

insert into items(name, description, price, stock, category_id) values
     ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
     ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
     ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil Data dari Database

a. Mengambil data users

select id, name, email from users;

b. Mengambil data items

select * from items where price>1000000;

select * from items where name LIKE '%sang%';

c. Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database

update items set price = 2500000 where id=1;


